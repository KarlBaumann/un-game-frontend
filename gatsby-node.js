/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
  if (stage === 'build-html' || stage === 'develop-html') {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /jspdf/,
            use: loaders.null(),
          },
          // {
          //   test: /gatsby-plugin-material-ui/,
          //   use: loaders.null(),
          // },
        ],
      },
    })
  }
}

const path = require(`path`)

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions
  return graphql(`
    {
      allWpSdg(
        sort: {fields: menuOrder, order: DESC}
        filter: {menuOrder: {ne: null}}
      ) {
        nodes {
          content
          slug
          title
          featuredImage {
            node {
              localFile {
                publicURL
              }
            }
          }
          SdgMeta {
            externeUrl
          }
        }
      }
      allWpPage {
        nodes {
          title
          content
          id
          slug
          featuredImage {
            node {
              localFile {
                publicURL
              }
            }
          }
        }
      }
    }
  `).then(result => {
    result.data.allWpSdg.nodes.forEach(node => {

      createPage({
        path: `/app/sdg/${node.slug}`,
        component: path.resolve(`./src/templates/sdg.js`),
        context: {
          title: node.title,
          content: node.content,
          slug: node.slug,
          featuredImage: node.featuredImage,
          externeUrl: node.SdgMeta.externeUrl
        },
      })
    })
    result.data.allWpPage.nodes.forEach(node => {
      createPage({
        path: node.slug,
        component: path.resolve(`./src/templates/page.js`),
        context: {
          title: node.title,
          content: node.content,
          slug: node.slug,
          featuredImage: node.featuredImage,
        },
      })
    })
  })
}

exports.onCreatePage = async ({ page, actions }) => {
  const { createPage } = actions

  // page.matchPath is a special key that's used for matching pages
  // only on the client.
  if (page.path.match(/^\/app/)) {
    page.matchPath = `/app/*`

    // Update the page.
    createPage(page)
  }
}

