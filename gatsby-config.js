require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
  siteMetadata: {
    title: `Der Wirkel`,
    description: `Der Wirkel, dein Tool, um was zu beWIRKen!`,
    author: `Anu, RennWest`,
  },
  plugins: [
    `gatsby-plugin-material-ui`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/static/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-gatsby-cloud`,
    //`gatsby-theme-material-ui`,
    {
      resolve: `gatsby-source-wordpress`,
      options: {
        /*
         * The full URL of the WordPress site's GraphQL API.
         * Example : 'https://www.example-site.com/graphql'
         */
        url: process.env.API_URL,
        type: {
          Page: {
            exclude: false,
          },
          Post: {
            exclude: true,
          },
          Menu: {
            exclude: true,
          },
          MenuItem: {
            exclude: true,
          },
          Player: {
            exclude: true,
          },
          User: {
            exclude: true,
          },
          UserRole: {
            exclude: true,
          },
          Tag: {
            exclude: true,
          },
          Category: {
            exclude: true,
          },
          Comment: {
            exclude: true,
          },
          PostFormat: {
            exclude: true,
          },
          ContentType: {
            exclude: true,
          },
          Team: {
            exclude: true,
          },
          MediaItems: {
            exclude: true,
          },
          // MediaItem: {
          //     exclude: true,
          // },
        },
      },
    },
    {
      resolve: 'gatsby-plugin-apollo',
      options: {
        uri: process.env.API_URL,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Der Wirkel`,
        short_name: `Wirkel`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#26BDE2`,
        display: `standalone`,
        icon: `static/images/icon.png`, // This path is relative to the root of the site.
        //cache_busting_mode: "none",
      },
    },
    {
      resolve: 'gatsby-plugin-offline',
      options: {
        precachePages: [`/*`, `/app/overview`],
      },
      // options: {
      //   workboxConfig: {
      //     globPatterns: ["**/src/images/*"],
      //   },
      // },
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /\.inline\.svg$/,
        },
      },
    }
  ],
}
