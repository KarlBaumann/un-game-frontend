import React, { useEffect, useState } from 'react'
import useLocalStorage from './useLocalStorage'
import { Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'
import DialogContentText from '@material-ui/core/DialogContentText'
import Button from '@material-ui/core/Button'
import useGameConfig from './useGameConfig'

function useCookieConsent () {
  const [open, setOpen] = useState(false)
  const [agreedToConsent, setAgreedToConsent] = useLocalStorage(process.env.COOKIE_CONSENT, false)
  const { gameConfig: { welcomePage } } = useGameConfig()

  useEffect(() => {
    setTimeout(()=>{
      setOpen(true)
    }, 3000)
  }, [])

  if (agreedToConsent) return null

  const handleClose = () => {
    setOpen(false)
  }

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>{welcomePage.cookiePopupTitle}</DialogTitle>
      <DialogContent>
        <DialogContentText dangerouslySetInnerHTML={{ __html: welcomePage.cookiePopup }}/>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => setAgreedToConsent(true)} color="primary" variant="contained" autoFocus>
          {welcomePage.cookiePopupAgreeButton}
        </Button>
      </DialogActions>
    </Dialog>
  )

}

export default useCookieConsent