import useUser from './useUser'
import { useEffect } from 'react'

function useSynchronizer () {
  const { syncStatus, user } = useUser()

  useEffect(() => {
    if ('notSynced' === syncStatus) {
      console.log('I would be syncing User now', user)
    }
  }, [syncStatus])

}

export default useSynchronizer