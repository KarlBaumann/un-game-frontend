import useIsClient from './useIsClient'

const useIsStandalone = () => {
  const { isClient } = useIsClient()

  function isStandalone () {
    if (!isClient) return null

    return (window.matchMedia('(display-mode: standalone)').matches)
      || (window.matchMedia('(display-mode: fullscreen)').matches)
      || (window.matchMedia('(display-mode: minimal-ui)').matches)
      || (window.navigator.standalone)
      || document.referrer.includes('android-app://')
  }

  return { isStandalone }
}

export default useIsStandalone