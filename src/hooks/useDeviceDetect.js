import React, { useEffect, useState } from 'react'
import useIsClient from './useIsClient'

function useDeviceDetect () {
  const [isMobile, setMobile] = useState(false)
  const [isFirefox, setFirefox] = useState(false)
  const [isEdge, setEdge] = useState(false)
  const [isChrome, setChrome] = useState(false)
  const { isClient } = useIsClient()


  useEffect(() => {

    const userAgent =
      typeof window.navigator === 'undefined' ? '' : navigator.userAgent
    const mobile = Boolean(
      userAgent.match(
        /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i
      )
    )
    setMobile(mobile)

// Firefox 1.0+
    setFirefox(typeof InstallTrigger !== 'undefined')

// Edge 20+
    setEdge(!!window.StyleMedia)

// Chrome 1 - 71
    setChrome(!!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime))

  }, [])

  if (!isClient) return null


  return [
    isMobile,
    isFirefox,
    isEdge,
    isChrome
  ]
}

export default useDeviceDetect