import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client'

export const client = new ApolloClient({
  link: new HttpLink({
    uri: process.env.API_URL,
    //credentials: 'include',
    // fetchOptions: {
    //     mode: 'no-cors',
    // },
  }),
  cache: new InMemoryCache()
})