import React from 'react'
import TuDus from '../overview/TuDus'
import useIsClient from '../../../hooks/useIsClient'
import useUser from '../../../hooks/useUser'
import { Alert } from '@material-ui/lab'
import useGameConfig from '../../../hooks/useGameConfig'
import Layout from "../../layout"

function Favorites () {
  const { isClient } = useIsClient()
  const { user } = useUser()
  const { gameConfig: { signedInConfig } } = useGameConfig()

  if (!isClient) return null

  return (
    <Layout pageName="Tu Du's">
      <div className="favorites">
        {user.tudus.length > 0 &&
        <TuDus limit={false}/>
        }
        {user.tudus.length === 0 &&
        <Alert
          severity="info"
          className="info-alert space-top-20"
        >
          {signedInConfig.emptyTudusList}
        </Alert>
        }
      </div>
    </Layout>
  )
}

export default Favorites
