import React, { useState } from "react"
import Popover from "@material-ui/core/Popover"
import { IconButton } from "@material-ui/core"
import InfoIcon from "@material-ui/icons/Info"
import CloseIcon from "@material-ui/core/SvgIcon/SvgIcon";

function SdgInfoIcon({ content }) {
  const [anchorEl, setAnchorEl] = useState(null)

  const handleClickInfo = event => {
    setAnchorEl(event.currentTarget)
  }

  const handleCloseInfo = () => {
    setAnchorEl(null)
  }

  const openInfo = Boolean(anchorEl)
  const id = openInfo ? "simple-popover" : undefined

  return (
    <>
      <Popover
        id={id}
        open={openInfo}
        anchorEl={anchorEl}
        onClose={handleCloseInfo}
        PaperProps={{
          style: { width: 300, padding: 10 },
        }}
      >
          <CloseIcon onClick={handleCloseInfo} style={{float: "right", cursor: "pointer"}} />
          <span
          dangerouslySetInnerHTML={{
            __html: content,
          }}
        />
      </Popover>
      <IconButton aria-label="info" onClick={handleClickInfo}>
        <InfoIcon className="sdg-info-icon" />
      </IconButton>
    </>
  )
}

export default SdgInfoIcon
