import React, { useState } from 'react'
import { Dialog, DialogActions, DialogContent, DialogTitle, LinearProgress, TextField } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import useTeam from '../../../hooks/useTeam'
import useGameConfig from '../../../hooks/useGameConfig'

function AddTeamDialog ({ open, handleClose }) {
  const [newTeamName, setNewTeamName] = useState('')
  const [errorMessage, setErrorMessage] = useState(null)
  const [loading, setLoading] = useState(null)
  const { add } = useTeam()
  const { texts: { notificationTexts } } = useGameConfig()


  const handleSubmit = e => {
    e.preventDefault()
    setLoading(true)

    add(newTeamName, setErrorMessage).then(() => {
        handleClose()
        setLoading(false)
      }
    ).catch(() => setLoading(false))
  }

  return (
    <Dialog
      open={open}
      maxWidth="sm"
      fullWidth={true}
      onClose={handleClose}
      aria-labelledby="new-team-dialog-title"
      aria-describedby="new-team-dialog-description"
    >
      <DialogTitle id="new-team-dialog-title">{notificationTexts.addGroup}</DialogTitle>
      <DialogContent>
        {!loading &&
        <form method="post" onSubmit={handleSubmit} color="primary">
          <TextField
            autoComplete="off"
            label={notificationTexts.addNewGroup}
            placeholder={notificationTexts.nameOfGroup}
            required={true}
            fullWidth
            onChange={e => {
              setNewTeamName(e.target.value)
            }}
            helperText={errorMessage}
            error={errorMessage !== null}
          />
        </form>
        }
        {loading && <LinearProgress/>}
      </DialogContent>
      <DialogActions>
        <Button variant="contained" onClick={handleSubmit} color="primary" autoFocus>
          Hinzufügen
        </Button>
        <Button variant="contained" onClick={handleClose}>
          {notificationTexts.close}
        </Button>
      </DialogActions>
    </Dialog>)
}

export default AddTeamDialog