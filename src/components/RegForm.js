import React, { useState } from 'react'
import { FormControl, FormHelperText, Input, InputLabel, LinearProgress } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import { v4 as uuidv4 } from 'uuid'
import { navigate } from 'gatsby'
import useGameConfig from '../hooks/useGameConfig'
import useSingleToast from '../hooks/useSingleToast'
import { useMutation } from '@apollo/client'
import { GET_CODE_MUTATION } from '../graphql/queries'
import useNetwork from '../hooks/useNetwork'
import Grid from '@material-ui/core/Grid'
import HCaptcha from '@hcaptcha/react-hcaptcha'

function RegForm () {
  const { texts: { notificationTexts } } = useGameConfig()
  const { gameConfig } = useGameConfig()
  const [errorMessage, setErrorMessage] = useState(null)
  const [code, setCode] = useState(null)
  const { addSingleToast } = useSingleToast()
  const [createPlayerFromNickName] = useMutation(GET_CODE_MUTATION)
  const isOnline = useNetwork()
  const [nickName, setNickName] = useState('')
  const [loading, setLoading] = useState(false)
  const [captchaLoading, setCaptchaLoading] = useState(true)
  const [token, setToken] = useState(null)

  function onCaptchaLoad(e){
    setCaptchaLoading(false)
    //alert('loaded')
  }


  return (
    <div className="registration">
      <h1>{gameConfig.welcomePage.registration}</h1>
      <p>{gameConfig.welcomePage.zugangscodeGenerierenModal}</p>
      {loading ? <LinearProgress/>
        :

        <FormControl disabled={!!code} error={!!errorMessage} fullWidth>
          <Grid container justify="space-between" alignItems="center">
            <Grid item className="nickname-input" md={6}>
              <InputLabel htmlFor="nickname">{gameConfig.welcomePage.chooseNick}</InputLabel>
              <Input autoFocus id="nickname" required value={nickName} onChange={event => {
                setNickName(event.target.value)
              }} fullWidth/>
            </Grid>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <Grid item className="captcha">
              <HCaptcha
                sitekey={process.env.RECAPTCHA_SITE_KEY}
                onVerify={setToken}
                onLoad={onCaptchaLoad}
                languageOverride="de"
              />
              {captchaLoading && <LinearProgress/>}
            </Grid>
          </Grid>
          {!!errorMessage && <FormHelperText>{errorMessage}</FormHelperText>}

          <Button
            disabled={captchaLoading}
            onClick={async () => {
              if (isOnline) {

                if (!token) {
                  addSingleToast(
                    <div>
                      <p>{gameConfig.welcomePage.recaptcha}</p>
                    </div>,
                    { appearance: 'error' })
                  setErrorMessage(gameConfig.welcomePage.recaptcha)
                  return null
                }

                setLoading(true)

                createPlayerFromNickName({
                  variables: {
                    clientMutationId: uuidv4(),
                    nickName: nickName,
                    recaptchaToken: token
                  },
                }).then(r => {
                  if (r.data.createPlayerFromNickName.success === false) {
                    addSingleToast(
                      <div>
                        <p>{r.data.createPlayerFromNickName.error}</p>
                      </div>,
                      { appearance: 'error' })
                    setErrorMessage(r.data.createPlayerFromNickName.error)
                    setLoading(false)
                    return null
                  }

                  addSingleToast(
                    <div>
                      <p>{notificationTexts.codeGenerated}</p>
                    </div>,
                    { appearance: 'success' })
                  setLoading(false)
                  setCode(r.data.createPlayerFromNickName.code)
                  setErrorMessage(null)
                  navigate('/registered', {
                    state: {
                      code: r.data.createPlayerFromNickName.code,
                      nickName
                    },
                  })
                  return null
                })

              } else {
                addSingleToast(
                  <div>
                    <p>{notificationTexts.mustBeOnlineToGenerateCode}</p>
                  </div>,
                  { appearance: 'error' })
              }
            }}
            variant="contained" color="primary"
            className="generate-code-button"
          >
            {notificationTexts.generateCode}
          </Button>
        </FormControl>

      }
    </div>
  )
}

export default RegForm